#include <ctype.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <stdlib.h>

struct termios original_termios;

void disableRawMode() {
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &original_termios);
}

void enableRawMode() {
	tcgetattr(STDIN_FILENO, &original_termios);

	struct termios raw = original_termios;
	raw.c_iflag = raw.c_iflag & ~(IXON | ICRNL);
	raw.c_lflag = raw.c_lflag & ~(ECHO | ICANON | ISIG | IEXTEN);
	raw.c_oflag = raw.c_oflag & ~(OPOST);
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);

	atexit(disableRawMode);

}

int main() {
	enableRawMode();

	char userinput;
	
	while(read(STDIN_FILENO, &userinput, 1) == 1 && userinput != 'q') {
		if(iscntrl(userinput)) {
			printf("%d\r\n", userinput);
		} else {
			printf("%d ('%c')\r\n", userinput, userinput);
		}
	}
	
	return 0;
}
